class StaticPagesController < ApplicationController
  def home
    floor_model_user = User.find_by_role("floor_model_user") 
    # From rails api docs:
    # Model.find_by finds the first record matching some conditions.
    if floor_model_user.blank?
      @published_floor_model_activities = []
    else
      @published_floor_model_activities = Activity.where(user: floor_model_user, public_access: true)
    end
  end
end
