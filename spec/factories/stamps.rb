# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :stamp do
    icon "heart"
    event_datetime "2014-04-18 12:47:33"
    notes "MyText"
  end
end
