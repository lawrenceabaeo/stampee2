class ActivityPolicy < Struct.new(:user, :activity)
  def show?
    own? || public_floor_model_activity?(activity) 
  end

  def own?
    User.find(activity.user_id) == user
  end

  private

  def public_floor_model_activity?(activity)
    if ( (activity.user == User.find_by_role("floor_model_user")) && (activity.public_access == true) ) 
        return true
    else 
      return false
    end
  end

end
