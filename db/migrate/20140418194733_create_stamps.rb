class CreateStamps < ActiveRecord::Migration
  def change
    create_table :stamps do |t|
      t.text :icon
      t.datetime :event_datetime
      t.text :notes

      t.timestamps
    end
  end
end
