class AddDefaultStampIconToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :default_stamp_icon, :text
  end
end
