class ActivitiesController < ApplicationController
  before_filter :signed_in, except: [:show]
  add_breadcrumb "Activities", :activities_path

  def index
    @activities = Activity.where(:user => current_user)
  end

  def show
      @activity = Activity.find(params[:id])
      authorize @activity
      @stamps = @activity.stamps
      add_breadcrumb "This Activity", activity_path(@activity)
  end

  def new
    add_breadcrumb "Create New Activity", new_activity_path
    @activity = Activity.new
  end

  def edit
    @activity = Activity.find(params[:id])
    authorize @activity, :own?
    add_breadcrumb "This Activity", activity_path(@activity)
    add_breadcrumb "Edit Activity Settings", edit_activity_path(@activity)
  end

  def create
    @activity = current_user.activities.new(activity_params)
    if @activity.save
      flash[:notice] = "Success! Your activity was created!"
      redirect_to @activity
    else
      flash[:error] = "There was an error saving your activity. Please try again."
      render :new
    end
  end

  def stamp_create
    activity = Activity.find(params[:id])
    authorize activity, :show?
    default_stamp = activity.default_stamp_icon
    stamp = activity.stamps.new(:icon => default_stamp)
    if stamp.save
      flash[:notice] = "Success! You marked this activity!"
      redirect_to activity
    else
      flash[:error] = "There was an error marking your activity. Please try again."
      redirect_to activity
    end
  end

  def update
    @activity = Activity.find(params[:id])
    authorize @activity, :show?
    if @activity.update_attributes(activity_params)
      flash[:notice] = "Success! Your activity settings were updated."
      redirect_to @activity
    else
      flash[:error] = "Error saving activity. Please try again."
      render :edit
    end
  end

  def delete
    @activity = Activity.find(params[:id])
    authorize @activity, :show?
    add_breadcrumb "This Activity", activity_path(@activity)
    add_breadcrumb "Edit Activity Settings", edit_activity_path(@activity)
    add_breadcrumb "Delete This Activity", delete_activity_page_path(@activity)
  end

  def destroy
    @activity = Activity.find(params[:id])
    authorize @activity, :show?
    if @activity.destroy
      flash[:notice] = "Success! Your activity was deleted."
    else
      flash[:error] = "Your activity could not be deleted. Try again."
    end
    redirect_to @activity
  end

  private
  def activity_params
    params.require(:activity).permit(:name, :description, :image, :public_access, :default_stamp_icon)
  end

end
