require 'spec_helper'
require 'helpers/helper'

RSpec.configure do |c|
  c.include Helpers
end

feature 'User' do
  let(:email) { "fake1@example.com" }
  let(:password) { "helloworld" }
  let(:timezone1) { "Tijuana" }
  let(:timezone2) { "Arizona" }
  let(:user) { FactoryGirl.create(:user) }
  let!(:activity1) { FactoryGirl.create(:activity, user: user) }
  let!(:stamp1) { FactoryGirl.create(:stamp, activity: activity1) } 
  let(:create_new_activity_link) { "Create A New Activity" } 

  context "Registration" do
    before(:each) { visit new_user_registration_path }
    specify { expect(page).to have_field("user_time_zone") }
    describe "timezone" do 
      before(:each) do
        fill_in 'Email', with: email
        fill_in 'Password', with: password
        fill_in 'Password confirmation', with: password
        select timezone1, from: 'Time zone'
        click_button 'Sign up'
      end
      specify do
        expect(page).to have_content('A message with a confirmation link has been sent to your email address.')
        User.first.time_zone.should have_content(timezone1)
      end
    end # "timezone"
  end # context "registration"

  context "Edit" do 
    before(:each) do 
      visit root_path
      sign_in(user)
    end
    describe "form" do
      before { visit edit_user_registration_path }
      specify { expect(page).to have_field("user_time_zone") }
    end
    describe "timezone" do 
      context "stamp time display before edit" do 
        before { visit activity_path(1) }
        it "should have a stamp with correct time" do 
          expect(page).to have_content("#{Time.now.in_time_zone("Tijuana").strftime('%l')}:")
          # expect(page).to have_content("#{Time.now.in_time_zone("Samoa").strftime('%l')}:")
        end
      end # "stamp display before edit" 

      context "stamp time display after edit" do 
        before do
          visit edit_user_registration_path
          fill_in 'Current password', with: password
          select timezone2, from: 'Time zone'
          click_button 'Update'
          visit activity_path(1)
        end
        specify { User.first.time_zone.should have_content(timezone2) }
        specify do 
          expect(page).to have_content("#{Time.now.in_time_zone("Arizona").strftime('%l')}:")
        end
      end # "stamp display after edit" 
      pending "time display on individual stamp page, but that's very low priority."
    end # "editing"
  end # "timezone" 
  
  context "Cancel" do
    before(:each) do 
      visit root_path
      sign_in(user)
      visit edit_user_registration_path
    end

    specify { expect(page).to have_selector(:link_or_button, "Cancel my account") } 

    it "should let user delete account", js: true do
      page.driver.accept_js_confirms!
      click_button("Cancel my account")
      expect(page).to have_content("Bye! Your account was successfully cancelled.")
    end
    
  end # context "Cancel"

  context "Login" do 
    context "No referrer" do 
      before do 
        visit root_path
        sign_in(user)
      end
      it "should redirect to Activities page" do 
        expect(page).to have_link(create_new_activity_link) # back 2 activities
      end
    end # context "No referrer"

    context "from specific activity page" do 
      before do 
        visit activity_path(1)
        fill_in 'Email', with: user[:email]
        fill_in 'Password', with: "helloworld" # yup, hardcoding this here, I have a delicious bookmark somwhere explaining why
        click_button 'Sign in'  
      end
      pending "redirect back to page that asks for login"
      # NOTE: I had trouble figuring out how to implement both redirecting
      # to a default page AND redirecting to a specific page that asks user to login. 
      # Will just redirect to the activities page for now. 
      # There ARE solutions for both, but one requires an authentication filter that
      # I do NOT use, since I am using pundit.
       #specify { expect(page).to have_selector(:link_or_button, 'Add Stamp!') }
    end

  end # context "Login"

end # feature "User"

