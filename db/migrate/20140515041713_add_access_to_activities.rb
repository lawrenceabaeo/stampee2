class AddAccessToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :public_access, :boolean, default: false
    Activity.update_all public_access: false
  end
end
