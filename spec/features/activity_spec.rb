require 'spec_helper'
require 'helpers/helper'

RSpec.configure do |c|
  c.include Helpers
end

feature 'Activity' do
  let(:user) { FactoryGirl.create(:user) }
  let(:create_new_activity_link) { "Create A New Activity" } 
  let(:activities_index_link) { "Activities" }
  let!(:activity1) { FactoryGirl.create(:activity, user: user) }
  let!(:user2) { FactoryGirl.create(:user, email: "user2@example.com") }
  let!(:activity2) do
    FactoryGirl.create(:activity, user: user2, name: 'User2')
  end
  let!(:stamp1) { FactoryGirl.create(:stamp, activity: activity1) } 

  # a bunch of GUI things to look for
  #let(:stamp_icon_value) { "Roundicons-01.png" }
  let(:stamp_icon_value) { "fa fa-heart fa-3x" }
  let(:settings) { "Settings" }
  let(:delete_activity_link) { "Delete This Activity" }
  let(:delete_button) { "Delete" }
  let(:delete_warning) { "Do you want to delete this activity? This action cannot be undone." }
  let(:image_file) { "testimage1.png" }
  let(:my_test_image) { "spec/fixtures/myfiles/" + image_file } 
  let(:image_file2) { "testimage2.png" }
  let(:my_test_image2) { "spec/fixtures/myfiles/" + image_file2 } 
  let(:total_stamps_text) { "Total Notches" }
  let(:last_stamped_text) { "Last Marked" }
  let(:first_stamped_text) { "First Marked" }
  let(:public_private_option_text) { "Public access" }
  let(:home_page_text) { "Your World, Counted" }
  let(:sticker_font_icon_css) { ".fa-heart" }
  let(:add_stamp_button_text) { "Mark This Activity!" }
  let(:leaf_font_icon_css) { ".fa-leaf" }
  let(:activities_breadcrumb_text) { "Activities" }
  let(:individual_activity_breadcrumb_text) { "This Activity" } 
  let(:edit_activity_breadcrumb_text) { "Edit Activity Settings" }
  let(:create_activity_breadcrumb_text) { "Create New Activity" }
  let(:delete_activity_breadcrumb_text) { "Delete This Activity" }
  let(:individual_stamp_breadcrumb_text) { "This Notch" }
  let(:delete_notch_button_text) { "Delete This Notch" }

  context 'as logged in user' do
    before(:each) do
      visit root_path
      sign_in(user) 
    end

    describe "Index page" do
      context "without saved activities" do
        it "should have correct text and link" do
          Activity.find(activity1).destroy
          click_link(activities_index_link)
          expect(page).to have_content("You don't have any activities yet!")
          expect(page).to have_link(create_new_activity_link) 
        end
        it "should NOT show activity entries of other users" do
          expect(page).to_not have_link("User2")
        end
      end # context "without saved activities" do

      context "WITH a saved actvity" do
        before(:each) do
          click_link(activities_index_link)
        end

        it "should show the create link" do
          expect(page).to have_link(create_new_activity_link) 
        end

        it "should show activity entry for current user and NOT for others" do
          expect(page).to have_link("My Activity")
          expect(page).to_not have_link("User2")
        end
      end # context "WITH a saved actvity" do
    end # describe "Index page" do

    describe "Create page" do
      before do
        click_link(activities_index_link)
        click_link(create_new_activity_link)
      end        

      it "should have various form fields" do
        expect(page).to have_field('Name')
        expect(page).to have_field('Description')
        expect(page).to have_content('Image') 
        expect(page).to have_content('Notch Icon')
        expect(page).to have_content(create_activity_breadcrumb_text)
      end

      it "lets user create a new activity that is added to database" do
        fill_in 'Name', with: 'Rails Work!'
        fill_in 'Description', with: 'Stamp the times I do rails work!'
        expect{click_on ('Create')}.to change{user.activities.count}.by(1)
        expect(page).to have_content('Success!') 
      end

      it "lets user upload picture" do 
        fill_in 'Name', with: 'Rails Work!'
        fill_in 'Description', with: 'Stamp the times I do rails work!'
        attach_file "activity_image", my_test_image
        click_on('Create')
        expect(find_by_id("activity-image")[:alt]).to have_content("estimage1")
        # Yeah, I had to hard code the alt expecatation. Looks like the image name
        # gets transformed when it's turned into an alt tag
      end

      it "should require name" do 
        fill_in 'Description', with: 'Stamp the times I do rails work!'
        click_on('Create')
        expect(page).to have_content("can't be blank")
      end

      it "should limit name length to 100" do 
        fill_in 'Name', with: "a" * 101
        fill_in 'Description', with: 'Stamp the times I do rails work!'
        click_on('Create')
        expect(page).to have_content("too long")
      end

    end # describe "Create page" do

    describe "Individual Activity Page" do 

      describe "breadcrumbs" do 
        before(:each) do
          visit activity_path(activity1)
        end
        it "should have breadcrumb" do 
          # NOTE: Need way of figuring out between navigation 'Activities' link and the breadcrumb 'Activities' link
          # Eh. I couldn't figure out a way. The gem doesn't put an identifier in there. I could check if the breadcrumb div is present, but that seems to be just a half step
          expect(page).to have_link(activities_breadcrumb_text)
        end
      end

      context "Metrics Section" do 
        before(:each) do
          visit activity_path(activity1)
        end
        it "should have various fields" do 
          expect(page).to have_content(total_stamps_text)
          expect(page).to have_content(last_stamped_text)
          expect(page).to have_content(first_stamped_text)
        end
      end # context "Metrics Section" do 

      context "without stamps" do
        before(:each) do 
          Stamp.destroy(stamp1)
          click_link(activities_index_link)
          click_link(activity1.name) 
        end

        it "should show activity name, description and a new stamp button" do
          expect(page).to have_content(activity1.name)
          expect(page).to have_content(activity1.description)
          expect(page).to have_selector(:link_or_button, add_stamp_button_text)
          expect(page).to_not have_content(Time.now.strftime("%m/%d"))
        end

        it "should NOT have any stamps" do 
          expect(page).to_not have_css(sticker_font_icon_css)
        end

        it "should let user make a stamp" do 
          expect{click_on (add_stamp_button_text)}.to change{activity1.stamps.count}.by(1)
          expect(page).to have_content("Success! You marked this activity!")
        end
      end # context "without stamps" do

      context "with stamps" do
        before(:each) do
          click_link(activities_index_link)
          click_link(activity1.name) 
        end

        it "should show activity name, description and a new stamp button, and some time stuff" do
          expect(page).to have_content(activity1.name)
          expect(page).to have_content(activity1.description)
          expect(page).to have_selector(:link_or_button, add_stamp_button_text)
          expect(page).to have_content(Time.now.strftime("%m/%d"))
          expect(page).to have_content(Time.now.strftime("%l:")) # This should pass expect for the unusual case where this test is run around the top of the hour (example, FactoryGirl makes the sticker at 9:59, but this example is run at 10:00). 
        end

        it "should display a clickable stamp" do
          expect(page).to have_css(sticker_font_icon_css)
          expect(page).to have_link("", activity_stamp_path(activity1, stamp1))
        end

        context "click existing stamp" do
          before(:each) do 
            my_link = find(:xpath, "//a[contains(@href, '#{activity_stamp_path(activity1, stamp1)}')]")
            my_link.click 
          end
          it "should display individual stamp page" do 
            expect(page).to have_content("This notch was added")
          end
        end

        it "should let user make a stamp" do 
          expect{click_on (add_stamp_button_text)}.to change{activity1.stamps.count}.by(1)
          expect(page).to have_content("Success! You marked this activity!")
        end

        context "Metrics Section" do 
          it "should show values" do 
            expect(page).to have_css('#total-stamps-value', :text => '1')
            expect(page).to have_content('ago')
            expect(page).to have_content(Time.now.strftime("%b %d, %Y"))
          end
        end # context "Metrics Section" do 

      end # context "with stamps" 


    end # describe "Show Activity page" do 

    describe "Individual Stamp page" do 
      before(:each) do
        click_link(activities_index_link)
        click_link(activity1.name) 
        my_link = find(:xpath, "//a[contains(@href, '#{activity_stamp_path(activity1, stamp1)}')]")
        my_link.click 
      end

      it "should show breadcrumbs" do 
        expect(page).to have_link(activities_breadcrumb_text)
        expect(page).to have_link(individual_activity_breadcrumb_text)
        expect(page).to have_content(individual_stamp_breadcrumb_text)
      end

      it "should show date when stamp was entered" do 
        expect(page).to have_css(sticker_font_icon_css) # hard-coded. The factory uses heart
        expect(page).to have_content("This notch was added")
        expect(page).to have_content("minute") # I'm going to assume the spe
        # will add the stamp in under 
        # 45 minutes
        expect(page).to have_selector(:link_or_button, delete_notch_button_text)
      end

      it "should let user delete stamp" do
        expect{click_on (delete_notch_button_text)}.to change{activity1.stamps.count}.by(-1)
        # user should land back on Activity page. 
        expect(page).to have_selector(:link_or_button, add_stamp_button_text)
        expect(page).to have_content('Success! Your notch was deleted.') 
      end
    end # describe "Individual Stamp page" do 

    describe "Edit Activity" do 
      before(:each) do
        click_link(activities_index_link)
        click_link(activity1.name) 
        click_link(settings)
      end

      it "should show breadcrumbs" do 
        expect(page).to have_link(activities_breadcrumb_text)
        expect(page).to have_link(individual_activity_breadcrumb_text)
        expect(page).to have_content(edit_activity_breadcrumb_text)
      end

      it "should have update form" do 
        expect(page).to have_content('Settings')
        expect(page).to have_field('Name')
        expect(page).to have_field('Description')
        expect(page).to have_selector(:link_or_button, 'Save')
        expect(page).not_to have_content(public_private_option_text) 
      end

      it "should let user edit" do
        fill_in 'Name', with: 'EDITED NAME!'
        fill_in 'Description', with: 'Edited description!'
        choose('activity_default_stamp_icon_leaf') # HARDCODED!
        attach_file "activity_image", my_test_image2
        click_on('Save')
        expect(page).to have_content('Success!') 
        expect(page).to have_content('EDITED NAME!') 
        expect(page).to have_content('Edited description!')
        expect(page).to have_selector(:link_or_button, 'Settings')
        expect(find_by_id("activity-image")[:alt]).to have_content("estimage2")
        click_on(add_stamp_button_text)
        expect(page).to have_css(leaf_font_icon_css)
      end

      it "should have delete activity link" do 
        expect(page).to have_selector(:link_or_button, delete_activity_link)
      end

      it "should let cancel edit" do
        expect(page).to have_selector(:link_or_button, 'Cancel')
        click_link('Cancel')
        expect(page).to have_selector(:link_or_button, settings)
      end
    end # describe "Edit Activity" do 

    describe "Delete Activity" do 
      before(:each) do
        click_link(activities_index_link)
        click_link(activity1.name) 
        click_link(settings)
        click_link(delete_activity_link)
      end

      it "should show breadcrumbs" do 
        expect(page).to have_link(activities_breadcrumb_text)
        expect(page).to have_link(individual_activity_breadcrumb_text)
        expect(page).to have_content(edit_activity_breadcrumb_text)
        expect(page).to have_content(delete_activity_breadcrumb_text)
      end

      it "page should have warning and proper links" do 
        expect(page).to have_content(delete_warning)
        expect(page).to have_selector(:link_or_button, delete_button)
        expect(page).to have_selector(:link_or_button, "Cancel")
      end

      describe "deleting" do
        it "should say success, land on correct page and update db" do 
          expect(Stamp.where(:activity_id => activity1.id).count).to eq(1)
          expect{click_on (delete_button)}.to change{user.activities.count}.by(-1)
          expect(page).to have_content("Success!")
          expect(page).to have_link(create_new_activity_link) # back 2 activities
          expect(Stamp.where(:activity_id => activity1.id).count).to eq(0)
        end
      end
    end # describe "Delete Activity" do 
  end # context 'as logged in user' do 

  context 'as not logged in user' do
    before(:each) do
      visit root_path
      click_link(activities_index_link)
    end
    specify { expect(page).to have_content("You have to be logged in to view this content, or if you are logged in, you may not have authorisation.") }
  end

  context 'as non-owner user' do
    before(:each) do
      visit root_path
      sign_in(user2)
      visit activity_path(activity1)
    end

    describe "viewing activity that is not theirs" do 
      specify { expect(page).to have_content('You are not authorized to perform that action.')}
      specify { expect(page).to have_content(home_page_text) } # Should land on home
    end

    describe "viewing individual stamp that is not theirs" do 
      before(:each) do
        visit(activity_stamp_path(activity1, stamp1))
      end
      specify { expect(page).to have_content('You are not authorized to perform that action.')}
      specify { expect(page).to have_content(home_page_text) } # Should land on home
    end
  end # context 'as non-owner user' do

end # feature 'Activity' do
