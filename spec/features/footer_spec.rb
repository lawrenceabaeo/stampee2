require 'spec_helper'
require 'helpers/helper'

RSpec.configure do |c|
  c.include Helpers
end

feature 'footer' do
  let(:brand_name) { "Stampee2" }
  before(:each) do
    visit root_path
  end
  it "should have a footer" do 
    expect(page).to have_content("© " + Time.now.year.to_s + " " + brand_name)
  end
end
