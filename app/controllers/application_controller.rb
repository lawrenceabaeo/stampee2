class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include Pundit

  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  before_action :configure_permitted_parameters, if: :devise_controller?
  
  around_filter :user_time_zone, :if => :current_user

  def signed_in
    unless user_signed_in?
      flash[:error] = "You have to be logged in to view this content, or if you are logged in, you may not have authorisation."
      # Copied this message from: 
      # http://www.earlychildhoodireland.ie/member-only-access/
      redirect_to new_user_session_path
    end
  end

  def user_time_zone(&block)
    if current_user.time_zone != "" || nil
      Time.use_zone(current_user.time_zone, &block)
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :time_zone
    devise_parameter_sanitizer.for(:account_update) << :time_zone
  end

  def after_sign_in_path_for(resource)
    activities_path
  end

  private

  def user_not_authorized
    flash[:error] = "You are not authorized to perform that action."
    redirect_to(request.referrer || root_path)
  end

  end # class ApplicationController < ActionController::Base
