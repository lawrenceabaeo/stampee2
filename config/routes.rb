Stampee2::Application.routes.draw do
  resources :activities do 
    member do 
      post :stamp_create
    end
    resources :stamps, only: [:show, :destroy]
  end
  get "/activities/:id/delete(.:format)", to: "activities#delete", as: 'delete_activity_page'
  devise_for :users
  get "static_pages/home"
  root "static_pages#home"
end
