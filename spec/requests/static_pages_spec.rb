require 'spec_helper'

describe "Static Pages" do
  let(:home_page_text) { "Your World, Counted" }
  describe "Home page" do 

    it "should have the content" do
      visit root_path
      expect(page).to have_content(home_page_text)
    end
  end
end
