class Activity < ActiveRecord::Base
  belongs_to :user
  has_many :stamps, dependent: :destroy
  validates :name, length: { maximum: 100 }, presence: true
  mount_uploader :image, ImageUploader 

  protected # ==========================================================
  def self.total_stamps(activity)
    Activity.find(activity).stamps.count
  end

  def self.last_stamp(activity)
  end

  def self.first_stamp(acitivity)
  end
end
