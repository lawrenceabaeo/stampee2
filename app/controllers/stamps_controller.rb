class StampsController < ApplicationController
  before_filter :signed_in, except: [:show]
  def show
    @stamp = Stamp.find(params[:id])
    @activity = Activity.find(params[:activity_id])
    authorize @activity, :show?
    add_breadcrumb "Activities", activities_path
    add_breadcrumb "This Activity", activity_path(@activity)
    add_breadcrumb "This Notch", activity_stamp_path(@activity, @stamp)
  end

  def destroy
    @stamp = Stamp.find(params[:id])
    @activity = Activity.find(params[:activity_id])
    authorize @activity, :show?
    if @stamp.destroy
      flash[:notice] = "Success! Your notch was deleted."
    else
      flash[:error] = "Your notch could not be deleted. Try again."
    end
    redirect_to @activity
  end
end
