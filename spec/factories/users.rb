# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    name "Factory Girl User"
    email "factorygirl@factorygirl.com"
    password "helloworld"
    password_confirmation "helloworld"
    confirmed_at Time.now
    time_zone "Tijuana"
  end
end
