module ActivitiesHelper
  def time_ago(stamp)
    if (stamp.blank?)
      ""
    else
      distance_of_time_in_words(Time.now, stamp.created_at) + " ago"
    end
  end

  def first_stamped(stamps)
    if (stamps.blank?)
      ""
    else
      t = stamps.order(created_at: :asc).first
      t.created_at.strftime("%b %d, %Y")
    end
  end

end
