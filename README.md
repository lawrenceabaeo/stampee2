#Stampee2 beta! - Count what matters to *you*

[http://stampee2-taiga-6069.herokuapp.com](http://stampee2-taiga-6069.herokuapp.com)

###UPDATE June 2014 
* Future development will be private. This code remains to let potential employers/clients/peers review it.
* Updated versions of this project can be seen at [www.hotnotch.com](www.hotnotch.com)

##Description: 
This ruby on rails application lets users create a page that can be marked (notched). These pages and their notches are saved to the user’s account, and the app displays select, basic meta-data about those notches. An example is a page created to mark the number of times a person spots a deer on the road. The user presses the ‘Mark This Activity!’ button, and a ‘notch’ will appear on the page, accompanied by the time/date the notch was recorded. The page will update the total notches that have accumulated.

##Technical Highlights
* Rails 4.0.2
* devise
* pundit
* figaro
* bootstrap
* bootstrap amelia theme
* carrier wave, fog and AWS for images
* capybara and rspec
* using breadcrumbs_on_rails
