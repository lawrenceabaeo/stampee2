require 'spec_helper'
require 'helpers/helper'

RSpec.configure do |c|
  c.include Helpers
end

feature 'floor model stuff' do
  let(:user) { FactoryGirl.create(:user) }
  let(:create_new_activity_link) { "Create A New Activity" } 
  let(:activities_index_link) { "Activities" }
  let!(:activity1) { FactoryGirl.create(:activity, user: user) }
  let!(:user2) { FactoryGirl.create(:user, email: "user2@example.com") }
  let!(:activity2) do
    FactoryGirl.create(:activity, user: user2, name: 'User2')
  end
  let!(:stamp1) { FactoryGirl.create(:stamp, activity: activity1) } 
  let!(:floor_model_user) do
    FactoryGirl.create(:user, email: "floormodel1@doodoo.com", role: "floor_model_user") 
  end
  let!(:floor_model_name_text1) { "Awesome Example!" }
  let!(:floor_model_activity1) do 
    FactoryGirl.create(:activity, user: floor_model_user, name: floor_model_name_text1, public_access: true)
  end
  let!(:floor_model_stamp1) { FactoryGirl.create(:stamp, activity: floor_model_activity1) } 

  # a bunch of GUI things to look for
  let(:stamp_icon_value) { "Roundicons-01.png" }
  let(:settings) { "Settings" }
  let(:delete_activity_link) { "Delete This Activity" }
  let(:delete_button) { "Delete" }
  let(:delete_warning) { "Do you want to delete this activity? This action cannot be undone." }
  let(:image_file) { "testimage1.png" }
  let(:my_test_image) { "spec/fixtures/myfiles/" + image_file } 
  let(:image_file2) { "testimage2.png" }
  let(:my_test_image2) { "spec/fixtures/myfiles/" + image_file2 } 
  let(:total_stamps_text) { "Total Notches" }
  let(:last_stamped_text) { "Last Marked" }
  let(:first_stamped_text) { "First Marked" }
  let(:public_private_option_text) { "Public access" }
  let(:home_page_text) { "Your World, Counted" }
  let(:floor_model_name_text2) { "Floor Name!" }
  let(:public_access_checkbox_text) { "Public access" }
  let(:stamp_page_text) { "This notch" }


  context 'as floor model user' do 
    before(:each) do 
      visit root_path
      sign_in(floor_model_user)
    end

    describe "public/private option appears" do 
      before { visit new_activity_path }
      specify { expect(page).to have_content(public_private_option_text) }
    end

    it "let's user publsh to homepage" do 
      visit new_activity_path
      fill_in 'Name', with: floor_model_name_text2
      fill_in 'Description', with: 'Stamp the times I do rails work!'
      check public_access_checkbox_text
      click_on 'Create'
      visit root_path
      expect(page).to have_content(floor_model_name_text2)
    end
  end # context 'as floor-model user' do 

  describe "regular users" do 
    before(:each) do 
      visit root_path
      sign_in(user)
      visit root_path
    end

    specify { expect(page).to have_content(floor_model_name_text1) }

    describe "click floor model activity" do 
      before(:each) do 
        click_on floor_model_name_text1
      end
      it "should show the individual activity page" do 
        expect(page).to have_content(floor_model_name_text1) 
        expect(page).to have_content(total_stamps_text)
      end
      it "should NOT have edit stuff" do 
        expect(page).not_to have_content('settings')
        expect(page).not_to have_selector(:link_or_button, 'Add Stamp!')
        expect(page).not_to have_selector(:link_or_button, 'Edit Activity Settings')
      end
    end # describe "click individual activity" do 

    describe "click floor model stamp" do 
      before(:each) do
        click_on floor_model_name_text1
        my_link = find(:xpath, "//a[contains(@href, '#{activity_stamp_path(floor_model_activity1, floor_model_stamp1)}')]")
        my_link.click 
      end
      it "should show the individual stamp page" do 
        expect(page).to have_content(stamp_page_text)
      end
      it "should NOT have delete stamp button" do 
        expect(page).not_to have_selector(:link_or_button, 'Delete This Stamp!')
      end
    end
  end # describe "regular users" do 

  
  describe "not logged in users" do 
    before(:each) do 
      visit root_path
    end

    specify { expect(page).to have_content(floor_model_name_text1) }

    describe "click floor model activity" do 
      before(:each) do 
        click_on floor_model_name_text1
      end
      it "should show the individual activity page" do 
        expect(page).to have_content(floor_model_name_text1) 
        expect(page).to have_content(total_stamps_text)
      end
      it "should NOT have edit stuff" do 
        expect(page).not_to have_content('settings')
        expect(page).not_to have_selector(:link_or_button, 'Add Stamp!')
      end
    end # describe "click individual activity" do 

    describe "click floor model stamp" do 
      before(:each) do
        click_on floor_model_name_text1
        my_link = find(:xpath, "//a[contains(@href, '#{activity_stamp_path(floor_model_activity1, floor_model_stamp1)}')]")
        my_link.click 
      end
      it "should show the individual stamp page" do 
        expect(page).to have_content(stamp_page_text)
      end
      it "should NOT have delete stamp button" do 
        expect(page).not_to have_selector(:link_or_button, 'Delete This Stamp!')
      end
    end
  end # describe "not logged in users" do 

end # feature 'floor model stuff' do 
