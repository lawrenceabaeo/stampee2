require 'spec_helper'
require 'helpers/helper'

RSpec.configure do |c|
  c.include Helpers
end

describe "API" do
  let(:user) { FactoryGirl.create(:user) }
  let(:create_new_activity_link) { "Create A New Activity" } 
  let(:activities_index_link) { "Activities" }
  let!(:activity1) { FactoryGirl.create(:activity, user: user) }
  let!(:user2) { FactoryGirl.create(:user, email: "user2@example.com") }
  let!(:activity2) do
    FactoryGirl.create(:activity, user: user2, name: 'User2')
  end
  let!(:stamp1) { FactoryGirl.create(:stamp, activity: activity1) } 
  let(:stamp_icon_value) { "Roundicons-01.png" }
  let(:auth_needed_message) { "You are not authorized to perform that action." }
  describe "stamp api" do 
    context "as a non-owner" do 
      before(:each) do 
        post_via_redirect user_session_path, 'user[email]' => user2.email,
          'user[password]' => user2.password
      end

      describe "deleting a stamp that is not theirs" do 
        before do 
          delete activity_stamp_path(activity1, stamp1) 
          follow_redirect!
        end
        specify { expect(activity1.stamps.count).to eq 1 }
        specify { expect(response.body).to include(auth_needed_message) }
      end

      describe  "stamping an activity that is not theirs" do 
        before do 
          post stamp_create_activity_path(activity1)
          follow_redirect!
        end
        specify { expect(activity1.stamps.count).to eq 1 }
        specify { expect(response.body).to include(auth_needed_message) }
      end

      describe  "updating an activity that is not theirs" do 
        before do 
          put(activity_path(activity1), id: activity1.id, activity: activity1.attributes = { :name => "FAKE", :description => "also fake" } )
          follow_redirect!
        end
        specify { expect(response.body).to include(auth_needed_message) }
      end  # describe  "updating an activity that is not theirs" do 
    end # "as a non-owner"
  end # "stamp api"

  describe "delete user" do 
    context "before deleting" do
      specify { User.count.should eq 2 } # we created two users
      specify { Activity.count.should eq 2 }
      specify { Stamp.count.should eq 1 }
    end # "before deleting"
    context "as the user deleting themselves" do  
      before(:each) do 
        post_via_redirect user_session_path, 'user[email]' => user.email,
          'user[password]' => user.password
        delete user_registration_path
      end
      specify { User.count.should eq 1 } # we created two users, only 1 deleted
      specify { Activity.count.should eq 1 }
      specify { Stamp.count.should eq 0 }
    end # "as the user" 
  end # "delete user"
end # "API"
